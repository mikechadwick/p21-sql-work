CREATE TABLE dbo.prod_ord_tickler_tjs
	(
	 uid INT IDENTITY
	,prod_ord_number DECIMAL
	,item VARCHAR(40)
	,processed VARCHAR(1) DEFAULT 'N'
	);