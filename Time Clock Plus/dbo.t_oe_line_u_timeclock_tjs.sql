SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- **********************************************************************
-- $Workfile: dbo.t_oe_line_u_timeclock_tjs.sql $
-- AUTHOR       : $Author: mikechadwick $
-- DATE CREATED : $Date: 2015-04-09 11:49:39-04:00 $ 
-- REVISION     : $Revision: 1 $
-- DESCRIPTION  : Script to create if non existent then amend SQL Server
--                trigger dbo.t_oe_line_u_timeclock_tjs 
-- PURPOSE      : 
-- **********************************************************************'
--
--
--

IF OBJECT_ID(N't_oe_line_u_timeclock_tjs' , N'TR') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[t_oe_line_u_timeclock_tjs];
	END;
GO 
 
GO 
CREATE TRIGGER dbo.t_oe_line_u_timeclock_tjs ON dbo.oe_line
	WITH EXEC AS 'TJSNOW\administrator'
	FOR UPDATE
AS
	--AS 
	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;

    -- Insert statements for trigger here
		BEGIN
			UPDATE
				mccl
			SET	
				Status = '1'
				,mccl.UTCDateModified = GETUTCDATE()
			FROM
				Inserted
				INNER JOIN p21_view_oe_hdr AS pvoh ON pvoh.oe_hdr_uid = Inserted.oe_hdr_uid
				INNER JOIN p21_view_oe_line AS pvol ON pvol.oe_line_uid = Inserted.oe_line_uid
				INNER JOIN p21_view_oe_line_service AS pvols ON pvols.oe_line_uid = Inserted.oe_line_uid
				INNER JOIN p21_view_service_inv_mast AS pvsim ON pvsim.service_inv_mast_uid = pvols.service_inv_mast_uid
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.CostCode AS mccl ON LEFT(mccl.Description , 7) = Inserted.order_no
																				AND mccl.ExportAs = pvol.item_id
																				AND mccl.Notes = pvsim.serial_number
			WHERE
				LEFT(mccl.Description , 7) = Inserted.order_no
				AND mccl.ExportAs = pvol.item_id
				AND mccl.Notes = pvsim.serial_number
				AND (
					  Inserted.complete = 'Y'
					  OR Inserted.cancel_flag = 'Y'
					);
		
			UPDATE
				ccn
			SET	
				ccn.Status = 1
			FROM
				Inserted
				INNER JOIN p21_view_oe_hdr AS pvoh ON pvoh.oe_hdr_uid = Inserted.oe_hdr_uid
				INNER JOIN p21_view_oe_line AS pvol ON pvol.oe_line_uid = Inserted.oe_line_uid
				INNER JOIN p21_view_oe_line_service AS pvols ON pvols.oe_line_uid = Inserted.oe_line_uid
				INNER JOIN p21_view_service_inv_mast AS pvsim ON pvsim.service_inv_mast_uid = pvols.service_inv_mast_uid
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.CostCode AS mccl ON LEFT(mccl.Description , 7) = Inserted.order_no
																				AND mccl.ExportAs = pvol.item_id
																				AND mccl.Notes = pvsim.serial_number
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.CostCodeNode AS ccn ON ccn.CompanyRecordId = mccl.CompanyRecordId
																				   AND mccl.RecordId = ccn.CostCodeRecordId
			WHERE
				LEFT(mccl.Description , 7) = Inserted.order_no
				AND mccl.ExportAs = pvol.item_id
				AND mccl.Notes = pvsim.serial_number
				AND (
					  Inserted.complete = 'Y'
					  OR Inserted.cancel_flag = 'Y'
					);
		
		END;

	END;

-- $History: /TJS/Triggers/dbo.t_prod_order_line_u_timeclock_tjs.sql $
-- 
-- ****************** Version 1 ****************** 
-- User: mikechadwick   Date: 2015-04-09   Time: 11:49:39-04:00 
-- Updated in: /TJS/Triggers 
--
--
GO