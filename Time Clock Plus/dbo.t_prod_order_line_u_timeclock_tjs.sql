SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- **********************************************************************
-- $Workfile: dbo.t_prod_order_line_u_timeclock_tjs.sql $
-- AUTHOR       : $Author: mikechadwick $
-- DATE CREATED : $Date: 2015-04-09 11:49:39-04:00 $ 
-- REVISION     : $Revision: 1 $
-- DESCRIPTION  : Script to create if non existent then amend SQL Server
--                trigger dbo.t_prod_order_line_u_timeclock_tjs 
-- PURPOSE      : 
-- **********************************************************************'
--
--
--

IF OBJECT_ID(N't_prod_order_line_u_timeclock_tjs' , N'TR') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[t_prod_order_line_u_timeclock_tjs];
	END;
GO 
 
GO 
CREATE TRIGGER dbo.t_prod_order_line_u_timeclock_tjs ON dbo.prod_order_line
	WITH EXEC AS 'TJSNOW\administrator'
	FOR UPDATE
AS
	--AS 
	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;

    -- Insert statements for trigger here
		BEGIN
			UPDATE
				mccl
			SET	
				Status = 1
				,mccl.UTCDateModified = GETUTCDATE()
			FROM
				Inserted
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.CostCode AS mccl ON mccl.Notes = CAST(Inserted.prod_order_number AS VARCHAR(20))
					--INNER JOIN Inserted ON TimeClockPlus.dbo.MasterCostCodeList.CostNotes = Inserted.prod_order_number
			WHERE
				mccl.Notes = CAST(Inserted.prod_order_number AS VARCHAR(20))
				AND (
					  Inserted.completed = 'Y'
					  OR Inserted.cancel = 'Y'
					);
		
			UPDATE
				ccn
			SET	
				ccn.Status = 1
			FROM
				Inserted
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.CostCode AS cc ON CAST(Inserted.prod_order_number AS VARCHAR(20)) = cc.Notes
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.CostCodeNode AS ccn ON ccn.CompanyRecordId = cc.CompanyRecordId
																				   AND cc.RecordId = ccn.CostCodeRecordId
			WHERE
				cc.Notes = CAST(Inserted.prod_order_number AS VARCHAR(20))
				AND (
					  Inserted.completed = 'Y'
					  OR Inserted.cancel = 'Y'
					);
		
		END;

	END;

-- $History: /TJS/Triggers/dbo.t_prod_order_line_u_timeclock_tjs.sql $
-- 
-- ****************** Version 1 ****************** 
-- User: mikechadwick   Date: 2015-04-09   Time: 11:49:39-04:00 
-- Updated in: /TJS/Triggers 
--
--
GO