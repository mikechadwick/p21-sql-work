--DISABLE TRIGGER t_prod_order_line_i_timeclock_tjs ON dbo.prod_order_line;
DISABLE TRIGGER t_prod_order_line_i_prod_ord_tickler_tjs ON dbo.prod_order_line;
DISABLE TRIGGER t_prod_order_line_u_timeclock_tjs ON dbo.prod_order_line;
--DISABLE TRIGGER t_oe_hdr_i_prod_ord_tickler_tjs ON dbo.oe_hdr;
DISABLE TRIGGER t_oe_line_service_i_prod_ord_tickler_tjs ON dbo.oe_line_service;
DISABLE TRIGGER t_oe_line_service_u_timeclock_tjs ON dbo.oe_line_service;


--ENABLE TRIGGER t_prod_order_line_i_timeclock_tjs ON dbo.prod_order_line;
--ENABLE TRIGGER t_prod_order_line_u_timeclock_tjs ON dbo.prod_order_line;
--ENABLE TRIGGER t_oe_hdr_i_prod_ord_tickler_tjs ON dbo.oe_hdr;
--ENABLE TRIGGER t_oe_line_i_prod_ord_tickler_tjs ON dbo.oe_line;
--ENABLE TRIGGER t_oe_line_u_timeclock_tjs ON dbo.oe_line;