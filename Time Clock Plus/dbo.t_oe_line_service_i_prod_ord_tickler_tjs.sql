SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- **********************************************************************
-- $Workfile:$
-- AUTHOR       : $Author: $
-- DATE CREATED : $Date: $ 
-- REVISION     : $Revision:$
-- DESCRIPTION  : Script to create if non existent then amend SQL Server
--                trigger dbo.t_oe_line_service_i_prod_ord_tickler_tjs 
-- PURPOSE      : Log to the tickler table to so that the service order can be processed into TCP
-- **********************************************************************'
--
--
--

IF OBJECT_ID(N't_oe_line_service_i_prod_ord_tickler_tjs' , N'TR') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[t_oe_line_service_i_prod_ord_tickler_tjs];
	END;
GO 
 
GO 
CREATE TRIGGER dbo.t_oe_line_service_i_prod_ord_tickler_tjs ON dbo.oe_line_service
	FOR INSERT
AS
	--AS 
	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;

    -- Insert statements for trigger here
		INSERT	INTO service_ord_tickler_tjs
				(
				 prod_ord_number
				,item
				,serial
				,processed
				)
				SELECT DISTINCT
					pvol.order_no
				   ,pvim.item_id
				   ,pvsim.serial_number
				   ,'N'
				FROM
					Inserted --INNER JOIN p21_view_oe_hdr AS pvoh
					INNER JOIN p21_view_oe_line AS pvol ON pvol.oe_line_uid = Inserted.oe_line_uid
					INNER JOIN p21_view_oe_hdr AS pvoh ON pvoh.oe_hdr_uid = pvol.oe_hdr_uid
					INNER JOIN p21_view_inv_mast AS pvim ON pvim.inv_mast_uid = pvol.inv_mast_uid
					--*INNER JOIN p21_view_oe_line_service AS pvols ON pvols.oe_line_uid = Inserted.oe_line_uid
					INNER JOIN p21_view_service_inv_mast AS pvsim ON pvsim.service_inv_mast_uid = Inserted.service_inv_mast_uid
				WHERE
					pvoh.order_type = 1706
					AND COALESCE(pvol.complete,'N') = 'N'
					AND COALESCE(pvol.cancel_flag,'N') = 'N';


	END;

-- $History:$
--
--
GO