SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- **********************************************************************
-- $Workfile:$
-- AUTHOR       : $Author: $
-- DATE CREATED : $Date: $ 
-- REVISION     : $Revision:$
-- DESCRIPTION  : Script to create if non existent then amend SQL Server
--                trigger dbo.t_prod_order_line_i_prod_ord_tickler_tjs 
-- PURPOSE      : Log to the tickler table to so that the prod order can be processed into TCP
-- **********************************************************************'
--
--
--

IF OBJECT_ID(N't_prod_order_line_i_prod_ord_tickler_tjs' , N'TR') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[t_prod_order_line_i_prod_ord_tickler_tjs];
	END;
GO 
 
GO 
CREATE TRIGGER dbo.t_prod_order_line_i_prod_ord_tickler_tjs ON dbo.prod_order_line
	FOR INSERT
AS
	--AS 
	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;

    -- Insert statements for trigger here
		INSERT	INTO prod_ord_tickler_tjs
				(
				 prod_ord_number
				,item
				,processed
				)
				SELECT
					Inserted.prod_order_number
				   ,pvim.item_id
				   ,'N'
				FROM
					Inserted
					INNER JOIN p21_view_inv_mast AS pvim ON pvim.inv_mast_uid = Inserted.inv_mast_uid;


	END;

-- $History:$
--
--
GO