-- **********************************************************************
-- $Workfile:$
-- AUTHOR       : $Author: $
-- DATE CREATED : $Date: $
-- REVISION     : $Revision: $
-- DESCRIPTION  : Script to create if non existent then amend SQL Server
--                procedure tjs_service_ord_to_timeclockplus_sp
-- PURPOSE      : 
-- **********************************************************************
--
--

IF OBJECT_ID(N'tjs_service_ord_to_timeclockplus_sp' , N'P') IS NULL
	BEGIN
		EXEC ('CREATE PROCEDURE [dbo].[tjs_service_ord_to_timeclockplus_sp] AS BEGIN SELECT 1 END');
	END;
GO 
 
GO 
ALTER PROCEDURE [dbo].[tjs_service_ord_to_timeclockplus_sp]
	--@parameter_name AS scalar_data_type ( = default_value ), ...
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS --	statements

	WHILE EXISTS ( SELECT
					*
				   FROM
					P21..service_ord_tickler_tjs AS t
				   WHERE
					t.processed = 'N' )
		BEGIN
			DECLARE	@id INT;
			DECLARE	@prod DECIMAL;
			DECLARE	@item VARCHAR(50);
			DECLARE	@serial VARCHAR(50);
			SET @id = (
						SELECT TOP 1
							t.uid
						FROM
							P21..service_ord_tickler_tjs AS t
						WHERE
							t.processed = 'N'
						ORDER BY
							t.uid
					  );
			SET @prod = (
						  SELECT
							t.prod_ord_number
						  FROM
							P21..service_ord_tickler_tjs AS t
						  WHERE
							t.uid = @id
						);
			SET @item = (
						  SELECT
							t.item
						  FROM
							P21..service_ord_tickler_tjs AS t
						  WHERE
							t.uid = @id
						);
			SET @serial = (
							SELECT
								t.serial
							FROM
								P21..service_ord_tickler_tjs AS t
							WHERE
								t.uid = @id
						  );

			INSERT	INTO tcp_EmployeeWork.CostCode
					(
					 RecordId
					,Alias
					,BudgetMinutes
					,BudgetMoney
					,CompanyRecordId
					,DateStart
					,DateStop
					,Description
					,ExpiryOption
					,ExportAs
					,Flags
					,FullName
					,IsBillable
					,Name1
					,Name2
					,Name3
					,Name4
					,Name5
					,Notes
					,Stage
					,Status
					,UTCDateAdded
					,UTCDateModified
					)
					SELECT DISTINCT
						(
						  SELECT
							MAX(cc.RecordId) + 1
						  FROM
							tcp_EmployeeWork.CostCode AS cc
						)
					   ,NULL
					   ,NULL
					   ,NULL
					   ,1
					   ,NULL
					   ,NULL
				   --,CAST(CAST(pvoh.order_no AS VARCHAR(20)) + ' - ' + REPLACE(pvoh.ship2_name , '''' , '''''') AS VARCHAR(60))
					   ,CAST(CASE WHEN pvol.item_id = 'SERVICE REPAIR - CONTROL' THEN 'SR-C'
								  WHEN pvol.item_id = 'SERVICE REPAIR - TRANSFORMER' THEN 'SR-T'
								  WHEN pvol.item_id = 'SERVICE REPAIR' THEN 'SR'
								  WHEN pvol.item_id = 'SEMINAR - CUSTOMER' THEN 'SEM-C'
								  WHEN pvol.item_id = 'SERVICE CALL' THEN 'SC'
								  WHEN pvol.item_id = 'SERVICE REPAIR - BOARD' THEN 'SR-B'
							 END + ' - ' + REPLACE(pvoh.ship2_name , '''' , '''''') AS VARCHAR(60))
					   ,0
					   ,pvol.item_id
					   ,1
				 --  ,CASE WHEN pvol.item_id LIKE 'SERVICE REPAIR%' THEN 'SERVICE'
					--	 WHEN pvol.item_id LIKE 'SERVICE CALL%' THEN 'SERVICE'
					--	 WHEN pvol.item_id LIKE 'SEMINAR%' THEN 'SERVICE'
					--	 ELSE 'SERVICE'
					--END + '\' + ( ( CASE WHEN pvol.item_id = 'SERVICE REPAIR - CONTROL' THEN 'SR-C'
					--					 WHEN pvol.item_id = 'SERVICE REPAIR - TRANSFORMER' THEN 'SR-T'
					--					 WHEN pvol.item_id = 'SERVICE REPAIR' THEN 'SR'
					--					 WHEN pvol.item_id = 'SEMINAR - CUSTOMER' THEN 'SEM-C'
					--					 WHEN pvol.item_id = 'SERVICE CALL' THEN 'SC'
					--					 WHEN pvol.item_id = 'SERVICE REPAIR - BOARD' THEN 'SR-B'
					--				END ) + ' | ' + pvsim.serial_number )
					   ,CASE WHEN pvol.item_id LIKE 'SERVICE REPAIR%' THEN 'SERVICE'
							 WHEN pvol.item_id LIKE 'SERVICE CALL%' THEN 'SERVICE'
							 WHEN pvol.item_id LIKE 'SEMINAR%' THEN 'SERVICE'
							 ELSE 'SERVICE'
						END + '\' + ( CAST(pvoh.order_no AS VARCHAR(20)) + ' | ' + pvsim.serial_number )
					   ,0
					   ,CASE WHEN pvol.item_id LIKE 'SERVICE REPAIR%' THEN 'SERVICE'
							 WHEN pvol.item_id LIKE 'SERVICE CALL%' THEN 'SERVICE'
							 WHEN pvol.item_id LIKE 'SEMINAR%' THEN 'SERVICE'
							 ELSE 'SERVICE'
						END
				   --,CAST(CASE WHEN pvol.item_id = 'SERVICE REPAIR - CONTROL' THEN 'SR-C'
							--  WHEN pvol.item_id = 'SERVICE REPAIR - TRANSFORMER' THEN 'SR-T'
							--  WHEN pvol.item_id = 'SERVICE REPAIR' THEN 'SR'
							--  WHEN pvol.item_id = 'SEMINAR - CUSTOMER' THEN 'SEM-C'
							--  WHEN pvol.item_id = 'SERVICE CALL' THEN 'SC'
							--  WHEN pvol.item_id = 'SERVICE REPAIR - BOARD' THEN 'SR-B'
						 --END + ' | ' + pvsim.serial_number AS VARCHAR(50))
					   ,CAST(CAST(pvoh.order_no AS VARCHAR(20)) + ' | ' + pvsim.serial_number AS VARCHAR(50))
					   ,NULL
					   ,NULL
					   ,NULL
					   ,pvsim.serial_number
					   ,0
					   ,0
					   ,GETUTCDATE()
					   ,NULL
					FROM
						P21..p21_view_oe_hdr AS pvoh
						INNER JOIN P21..p21_view_oe_line AS pvol ON pvol.oe_hdr_uid = pvoh.oe_hdr_uid
						INNER JOIN P21..p21_view_oe_line_service AS pvols ON pvols.oe_line_uid = pvol.oe_line_uid
						INNER JOIN P21..p21_view_service_inv_mast AS pvsim ON pvsim.service_inv_mast_uid = pvols.service_inv_mast_uid
						INNER JOIN P21..p21_view_codes AS pvc ON pvols.row_status_flag = pvc.code_no
																 AND pvc.code_group_no = 1313
					WHERE
						pvoh.order_no = @prod
						AND pvol.item_id = @item
						AND pvsim.serial_number = @serial
					ORDER BY
						pvol.item_id;

			UPDATE
				P21..service_ord_tickler_tjs
			SET	
				P21..service_ord_tickler_tjs.processed = 'Y'
			WHERE
				P21..service_ord_tickler_tjs.uid = @id;
		END;


	DECLARE	@temp TABLE
		(
		 uid INT IDENTITY
		,ccri INT
		,processed VARCHAR(1) DEFAULT 'N'
		);

	INSERT	INTO @temp
			(
			 ccri
			,processed
			
			)
			SELECT
				cc.RecordId
			   ,'N'
			FROM
				tcp_EmployeeWork.CostCode AS cc
			WHERE
				cc.ExportAs IS NOT NULL
				AND cc.Flags = 1
				AND cc.RecordId NOT IN ( SELECT
											COALESCE(ccn.CostCodeRecordId , '')
										 FROM
											tcp_EmployeeWork.CostCodeNode AS ccn );

	WHILE EXISTS ( SELECT
					*
				   FROM
					@temp AS t
				   WHERE
					t.processed = 'N' )
		BEGIN
			DECLARE	@uid INT;
			DECLARE	@ccri INT;
			SET @uid = (
						 SELECT TOP 1
							t.uid
						 FROM
							@temp AS t
						 WHERE
							t.processed = 'N'
						 ORDER BY
							t.uid
					   );
			SET @ccri = (
						  SELECT
							t.ccri
						  FROM
							@temp AS t
						  WHERE
							t.uid = @uid
						);

			INSERT	INTO tcp_EmployeeWork.CostCodeNode
					(
					 RecordId
					,CompanyRecordId
					,CostCodeRecordId
					,Level
					,Name
					,ParentRecordId
				
					
					)
					SELECT DISTINCT
						(
						  SELECT
							MAX(ccn.RecordId) + 1
						  FROM
							tcp_EmployeeWork.CostCodeNode AS ccn
						)
					   ,1
					   ,cc.RecordId
					   ,2
					   ,cc.Name2
					   ,ccn2.RecordId
					FROM
						tcp_EmployeeWork.CostCode AS cc
						LEFT JOIN tcp_EmployeeWork.CostCodeNode AS ccn2 ON ccn2.CompanyRecordId = cc.CompanyRecordId
																		   AND cc.Name1 = ccn2.Name
					WHERE
						cc.RecordId = @ccri;
					
			UPDATE
				@temp
			SET	
				[@temp].processed = 'Y'
			WHERE
				[@temp].uid = @uid;
		END;
	
-- $History:$
--
--
GO