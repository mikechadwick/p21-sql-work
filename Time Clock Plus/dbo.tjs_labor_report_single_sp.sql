-- **********************************************************************
-- $Workfile: dbo.tjs_labor_report_single_sp.sql $
-- AUTHOR       : $Author: mikechadwick $
-- DATE CREATED : $Date: 2015-06-10 14:02:38-04:00 $
-- REVISION     : $Revision: 2 $
-- DESCRIPTION  : Script to create if non existent then amend SQL Server
--                procedure tjs_labor_report_single_sp
-- PURPOSE      : 
-- **********************************************************************
--
--

IF OBJECT_ID(N'tjs_labor_report_single_sp' , N'P') IS NULL
	BEGIN
		EXEC ('CREATE PROCEDURE [dbo].[tjs_labor_report_single_sp] AS BEGIN SELECT 1 END');
	END;
GO 
 
GO 
ALTER PROCEDURE [dbo].[tjs_labor_report_single_sp]
	--@parameter_name AS scalar_data_type ( = default_value ), ...
	@prod NVARCHAR(255)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
	WITH EXEC AS 'TJSNOW\Administrator'
AS --	statements
	
	BEGIN
		IF EXISTS ( SELECT
						*
					FROM
						tempdb.INFORMATION_SCHEMA.TABLES
					WHERE
						TABLES.TABLE_NAME = '##jobs' )
			DROP TABLE tempdb..##jobs;

	

		CREATE TABLE ##jobs
			(
			 cid INT IDENTITY
			,import_set_no VARCHAR(20)
			,company_id VARCHAR(20)
			,location_id VARCHAR(20)
			,technician_id INT
			,production_order_number INT
			,assembly_id VARCHAR(50)
			,component_id VARCHAR(50)
			,operation_type VARCHAR(40)
			,labor_id VARCHAR(40)
			,start_time DATETIME
			,end_time DATETIME
			,labor_hours VARCHAR(20)
			,rate VARCHAR(20)
			,processed VARCHAR(1)
			);
	END;
	INSERT	INTO ##jobs
			(
			 import_set_no
			,company_id
			,location_id
			,technician_id
			,production_order_number
			,assembly_id
			,component_id
			,operation_type
			,labor_id
			,start_time
			,end_time
			,labor_hours
			,rate
			,processed
			)
			SELECT DISTINCT
				eh.RecordId AS 'Import Set No'
			   ,'TJS' AS 'Company ID'
			   ,'10001' AS 'Location ID'
			 --  ,CASE WHEN mjcl.Description = 'ASSY' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'CMM' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'CRATE' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'EENG' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'EVALUATION' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'FAB' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'FLIGHT' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'INSPECT' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'MACH' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'MANUAL' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'MENG' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'MILEAGE' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'PAINT' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'PILOT' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'PREP' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'PROCESS' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'PROGRAM' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'PROJ' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'REPAIR' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'SERVICE' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'SERVICE REV' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'TEST' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'TRANS' THEN ecfd.Value
				--	 WHEN mjcl.Description = 'TRAVEL' THEN ecfd.Value
				--END AS 'Technician ID'
			   ,ecfd.Value AS 'Technician ID'
			   ,mccl.Notes AS 'Production Order Number'
			   ,mccl.ExportAs AS 'Assembly Item ID'
			   --,mjcl.Description AS 'Component ID'
			   ,CASE WHEN mjcl.Description = 'SERVICE REV' THEN 'SERVICE'
					 ELSE mjcl.Description
				END 'Component ID'
			   ,'LABOR' AS 'Operation Type'
			   --,mjcl.Description AS 'Labor ID'
			   ,CASE WHEN mjcl.Description = 'SERVICE REV' THEN 'SERVICE'
					 ELSE mjcl.Description
				END 'Labor ID'
			   ,eh.TimeIn AS 'Start Time'
			   ,eh.TimeOut AS 'End Time'
			   ,'' AS 'Labor Hours'
			   ,'Rate' AS 'Labory Type'
			   ,'N' AS 'Processed'
			FROM
				TimeClockPlus70.tcp_EmployeeWork.WorkSegment AS eh
				INNER JOIN TimeClockPlus70.tcp_Employee.Employee AS el ON eh.EmployeeRecordId = el.RecordId
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.EmployeeJobCode AS ejc ON eh.JobCodeRecordId = ejc.RecordId
																					  AND el.RecordId = ejc.EmployeeRecordId
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.JobCode AS mjcl ON ejc.JobCodeRecordId = mjcl.RecordId
				INNER JOIN TimeClockPlus70.tcp_EmployeeWork.CostCode AS mccl ON eh.CostCodeRecordId = mccl.RecordId  --mccl.Level1 + '\' + mccl.Level2
				--LEFT JOIN TimeClockPlus70.tcp_Company.CustomField AS ecf ON mjcl.Description = ecf.Name
				LEFT JOIN TimeClockPlus70.tcp_Company.CustomFieldValue AS ecfd ON ecfd.EmployeeRecordId = el.RecordId
																				  AND ecfd.CustomFieldRecordId = 16023552
			WHERE
				mccl.Notes = @prod
				AND (
					  COALESCE(eh.CostCodeRecordId , '') <> ''
					  AND eh.TimeOut IS NOT NULL
					)
				AND eh.ApprovedExceptions NOT IN ( 1024 , 1088 , 1152 , 1216 , 1536 , 1600 , 1664 , 1728 , 3072 , 3136 , 3200 , 3264 , 3584 , 3648 , 3712 , 3776 );

	WHILE EXISTS ( SELECT
					j.cid
				   FROM
					##jobs AS j
				   WHERE
					j.processed = 'N' )
		BEGIN
			DECLARE	@cid INT;
			SET @cid = (
						 SELECT
							MIN(##jobs.cid)
						 FROM
							##jobs
						 WHERE
							##jobs.processed = 'N'
					   );
			BEGIN
				UPDATE
					TimeClockPlus70.tcp_EmployeeWork.WorkSegment
				SET	
					WorkSegment.ApprovedExceptions = CASE WorkSegment.ApprovedExceptions
													   WHEN NULL THEN 1024
													   WHEN 0 THEN 1024
													   WHEN 64 THEN 1088
													   WHEN 128 THEN 1152
													   WHEN 192 THEN 1216
													   WHEN 512 THEN 1536
													   WHEN 576 THEN 1600
													   WHEN 640 THEN 1664
													   WHEN 704 THEN 1728
													   WHEN 2048 THEN 3072
													   WHEN 2112 THEN 3136
													   WHEN 2176 THEN 3200
													   WHEN 2240 THEN 3264
													   WHEN 2560 THEN 3584
													   WHEN 2624 THEN 3648
													   WHEN 2688 THEN 3712
													   WHEN 2752 THEN 3776
													 END
				   ,WorkSegment.ApprovedByOtherUserRecordId = '327680'
				WHERE
					WorkSegment.RecordId = (
											 SELECT
												j.import_set_no
											 FROM
												##jobs AS j
											 WHERE
												j.cid = @cid
										   );

				UPDATE
					##jobs
				SET	
					processed = 'Y'
				WHERE
					##jobs.cid = @cid;

			END;
		END;

	SELECT
		*
	FROM
		##jobs AS j;

	DECLARE	@tab CHAR(1) = CHAR(9);
	DECLARE	@sub VARCHAR(255) = 'Labor File For Production Order ' + CAST(@prod AS VARCHAR(20));
	DECLARE	@filename VARCHAR(255) = 'labor_file_for_' + CAST(@prod AS VARCHAR(20)) + '.txt.';


	EXEC msdb.dbo.sp_send_dbmail @profile_name = 'P21 Alerts' , @recipients = 'xxx@xxx.com' , -- varchar(max)
		@copy_recipients = '' , -- varchar(max)
		@blind_copy_recipients = '' , -- varchar(max)
		--@subject = N'Labor File' , -- nvarchar(255)
		@subject = @sub , @body = N'Here is your labor file to upload' , -- nvarchar(max)
		@query = N'SET NOCOUNT ON 
				SELECT
					j.import_set_no
					,j.company_id
					,j.location_id
					,j.technician_id
					,j.production_order_number
					,j.assembly_id
					,j.component_id
					,j.operation_type
					,j.labor_id
					,j.start_time
					,j.end_time
					,j.labor_hours
					,j.rate
				FROM
					##jobs AS j' , -- nvarchar(max)
		@attach_query_result_as_file = 1 , -- bit
		--@query_attachment_filename = N'labor.txt' , -- nvarchar(260)
		@query_attachment_filename = @filename , @query_result_header = 0 , -- bit
		@query_result_width = 32767 , -- int
		@query_result_separator = @tab , -- char(1)
		@exclude_query_output = 0 , -- bit
		@append_query_error = 0 , -- bit
		@query_no_truncate = 0 , -- bit
		@query_result_no_padding = 1;
  -- bit
 -- varchar(max)
		

-- $History: /TJS/Stored Procs/Stored Procs/dbo.tjs_labor_report_single_sp.sql $
-- 
-- ****************** Version 2 ****************** 
-- User: mikechadwick   Date: 2015-06-10   Time: 14:02:38-04:00 
-- Updated in: /TJS/Stored Procs/Stored Procs 
-- 
-- ****************** Version 1 ****************** 
-- User: mikechadwick   Date: 2015-06-10   Time: 08:43:10-04:00 
-- Updated in: /TJS/Stored Procs/Stored Procs 
-- Create SP to all for the generation of a labor file to import into P21 for 
-- production orders. 
--
--
GO
GRANT EXEC ON [dbo].[tjs_labor_report_single_sp] TO PxxiUser;
GRANT EXEC ON dbo.tjs_labor_report_single_sp TO [php-master];