DECLARE	@temp TABLE
	(
	 uid INT IDENTITY
	,prod_ord_num DECIMAL
	,item VARCHAR(50)
	,serial VARCHAR(50)
	,processed VARCHAR(1) DEFAULT 'N'
	);

INSERT	INTO @temp
		(
		 prod_ord_num
		,item
		,serial
		,processed
		)
		SELECT DISTINCT
			pvoh.order_no
		   ,pvol.item_id
		   ,pvsim.serial_number
		   ,'N'
		FROM
			P21..p21_view_oe_hdr AS pvoh
			INNER JOIN P21..p21_view_oe_line AS pvol ON pvol.oe_hdr_uid = pvoh.oe_hdr_uid
			INNER JOIN P21..p21_view_oe_line_service AS pvols ON pvols.oe_line_uid = pvol.oe_line_uid
			INNER JOIN P21..p21_view_service_inv_mast AS pvsim ON pvsim.service_inv_mast_uid = pvols.service_inv_mast_uid
			INNER JOIN P21..p21_view_codes AS pvc ON pvols.row_status_flag = pvc.code_no
													 AND pvc.code_group_no = 1313
		WHERE
			pvoh.order_type = 1706
			AND pvoh.completed = 'N'
			AND pvol.complete = 'N'
		ORDER BY
			pvoh.order_no
		   ,pvol.item_id;

WHILE EXISTS ( SELECT
				*
			   FROM
				@temp AS t
			   WHERE
				t.processed = 'N' )
	BEGIN
		DECLARE	@id INT;
		DECLARE	@prod DECIMAL;
		DECLARE	@item VARCHAR(50);
		DECLARE	@serial VARCHAR(50);
		SET @id = (
					SELECT TOP 1
						t.uid
					FROM
						@temp AS t
					WHERE
						t.processed = 'N'
					ORDER BY
						t.uid
				  );
		SET @prod = (
					  SELECT
						t.prod_ord_num
					  FROM
						@temp AS t
					  WHERE
						t.uid = @id
					);
		SET @item = (
					  SELECT
						t.item
					  FROM
						@temp AS t
					  WHERE
						t.uid = @id
					);
		SET @serial = (
						SELECT
							t.serial
						FROM
							@temp AS t
						WHERE
							t.uid = @id
					  );

		INSERT	INTO tcp_EmployeeWork.CostCode
				(
				 RecordId
				,Alias
				,BudgetMinutes
				,BudgetMoney
				,CompanyRecordId
				,DateStart
				,DateStop
				,Description
				,ExpiryOption
				,ExportAs
				,Flags
				,FullName
				,IsBillable
				,Name1
				,Name2
				,Name3
				,Name4
				,Name5
				,Notes
				,Stage
				,Status
				,UTCDateAdded
				,UTCDateModified
				)
				SELECT DISTINCT
					(
					  SELECT
						MAX(cc.RecordId) + 1
					  FROM
						tcp_EmployeeWork.CostCode AS cc
					)
				   ,NULL
				   ,NULL
				   ,NULL
				   ,1
				   ,NULL
				   ,NULL
				   ,CAST(CAST(pvoh.order_no AS VARCHAR(20)) + ' - ' + REPLACE(pvoh.ship2_name , '''' , '''''') AS VARCHAR(60))
				   ,0
				   ,pvol.item_id
				   ,1
				   ,CASE WHEN pvol.item_id LIKE 'SERVICE REPAIR%' THEN 'SERVICE'
						 WHEN pvol.item_id LIKE 'SERVICE CALL%' THEN 'SERVICE'
						 WHEN pvol.item_id LIKE 'SEMINAR%' THEN 'SERVICE'
						 ELSE 'SERVICE'
					END + '\' + ( ( CASE WHEN pvol.item_id = 'SERVICE REPAIR - CONTROL' THEN 'SR-C'
										 WHEN pvol.item_id = 'SERVICE REPAIR - TRANSFORMER' THEN 'SR-T'
										 WHEN pvol.item_id = 'SERVICE REPAIR' THEN 'SR'
										 WHEN pvol.item_id = 'SEMINAR - CUSTOMER' THEN 'SEM-C'
										 WHEN pvol.item_id = 'SERVICE CALL' THEN 'SC'
										 WHEN pvol.item_id = 'SERVICE REPAIR - BOARD' THEN 'SR-B'
									END ) + ' | ' + pvsim.serial_number )
				   ,0
				   ,CASE WHEN pvol.item_id LIKE 'SERVICE REPAIR%' THEN 'SERVICE'
						 WHEN pvol.item_id LIKE 'SERVICE CALL%' THEN 'SERVICE'
						 WHEN pvol.item_id LIKE 'SEMINAR%' THEN 'SERVICE'
						 ELSE 'SERVICE'
					END
				   ,CAST(CASE WHEN pvol.item_id = 'SERVICE REPAIR - CONTROL' THEN 'SR-C'
							  WHEN pvol.item_id = 'SERVICE REPAIR - TRANSFORMER' THEN 'SR-T'
							  WHEN pvol.item_id = 'SERVICE REPAIR' THEN 'SR'
							  WHEN pvol.item_id = 'SEMINAR - CUSTOMER' THEN 'SEM-C'
							  WHEN pvol.item_id = 'SERVICE CALL' THEN 'SC'
							  WHEN pvol.item_id = 'SERVICE REPAIR - BOARD' THEN 'SR-B'
						 END + ' | ' + pvsim.serial_number AS VARCHAR(50))
				   ,NULL
				   ,NULL
				   ,NULL
				   ,pvsim.serial_number
				   ,0
				   ,0
				   ,GETUTCDATE()
				   ,NULL
				FROM
					P21..p21_view_oe_hdr AS pvoh
					INNER JOIN P21..p21_view_oe_line AS pvol ON pvol.oe_hdr_uid = pvoh.oe_hdr_uid
					INNER JOIN P21..p21_view_oe_line_service AS pvols ON pvols.oe_line_uid = pvol.oe_line_uid
					INNER JOIN P21..p21_view_service_inv_mast AS pvsim ON pvsim.service_inv_mast_uid = pvols.service_inv_mast_uid
					INNER JOIN P21..p21_view_codes AS pvc ON pvols.row_status_flag = pvc.code_no
															 AND pvc.code_group_no = 1313
				WHERE
					pvoh.order_no = @prod
					AND pvol.item_id = @item
					AND pvsim.serial_number = @serial
				ORDER BY
					pvol.item_id;

		UPDATE
			@temp
		SET	
			[@temp].processed = 'Y'
		WHERE
			[@temp].uid = @id;
	END;

GO
DECLARE	@temp TABLE
	(
	 uid INT IDENTITY
	,ccri INT
	,processed VARCHAR(1) DEFAULT 'N'
	);

INSERT	INTO @temp
		(
		 ccri
		,processed
		)
		SELECT
			cc.RecordId
		   ,'N'
		FROM
			tcp_EmployeeWork.CostCode AS cc
		WHERE
			cc.ExportAs IS NOT NULL
			AND cc.Flags = 1;

WHILE EXISTS ( SELECT
				*
			   FROM
				@temp AS t
			   WHERE
				t.processed = 'N' )
	BEGIN
		DECLARE	@id INT;
		DECLARE	@ccri INT;
		SET @id = (
					SELECT TOP 1
						t.uid
					FROM
						@temp AS t
					WHERE
						t.processed = 'N'
					ORDER BY
						t.uid
				  );
		SET @ccri = (
					  SELECT
						t.ccri
					  FROM
						@temp AS t
					  WHERE
						t.uid = @id
					);

		INSERT	INTO tcp_EmployeeWork.CostCodeNode
				(
				 RecordId
				,CompanyRecordId
				,CostCodeRecordId
				,Level
				,Name
				,ParentRecordId
				
				)
				SELECT DISTINCT
					(
					  SELECT
						MAX(ccn.RecordId) + 1
					  FROM
						tcp_EmployeeWork.CostCodeNode AS ccn
					)
				   ,1
				   ,cc.RecordId
				   ,2
				   ,cc.Name2
				   ,ccn2.RecordId
				FROM
					tcp_EmployeeWork.CostCode AS cc
					LEFT JOIN tcp_EmployeeWork.CostCodeNode AS ccn2 ON ccn2.CompanyRecordId = cc.CompanyRecordId
																	   AND cc.Name1 = ccn2.Name
				WHERE
					cc.RecordId = @ccri;
					
		UPDATE
			@temp
		SET	
			[@temp].processed = 'Y'
		WHERE
			[@temp].uid = @id;
	END;