-- **********************************************************************
-- $Workfile:$
-- AUTHOR       : $Author: $
-- DATE CREATED : $Date: $
-- REVISION     : $Revision: $
-- DESCRIPTION  : Script to create if non existent then amend SQL Server
--                procedure tjs_prod_ord_to_timeclockplus_sp
-- PURPOSE      : 
-- **********************************************************************
--
--

IF OBJECT_ID(N'tjs_prod_ord_to_timeclockplus_sp' , N'P') IS NULL
	BEGIN
		EXEC ('CREATE PROCEDURE [dbo].[tjs_prod_ord_to_timeclockplus_sp] AS BEGIN SELECT 1 END');
	END;
GO 
 
GO 
ALTER PROCEDURE [dbo].[tjs_prod_ord_to_timeclockplus_sp]
	--@parameter_name AS scalar_data_type ( = default_value ), ...
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS --	statements

	WHILE EXISTS ( SELECT
					*
				   FROM
					P21..prod_ord_tickler_tjs AS t
				   WHERE
					t.processed = 'N' )
		BEGIN
			DECLARE	@id INT;
			DECLARE	@prod DECIMAL;
			DECLARE	@item VARCHAR(50);
			SET @id = (
						SELECT TOP 1
							t.uid
						FROM
							P21..prod_ord_tickler_tjs AS t
						WHERE
							t.processed = 'N'
						ORDER BY
							t.uid
					  );
			SET @prod = (
						  SELECT
							t.prod_ord_number
						  FROM
							P21..prod_ord_tickler_tjs AS t
						  WHERE
							t.uid = @id
						);
			SET @item = (
						  SELECT
							t.item
						  FROM
							P21..prod_ord_tickler_tjs AS t
						  WHERE
							t.uid = @id
						);

			INSERT	INTO tcp_EmployeeWork.CostCode
					(
					 RecordId
					,Alias
					,BudgetMinutes
					,BudgetMoney
					,CompanyRecordId
					,DateStart
					,DateStop
					,Description
					,ExpiryOption
					,ExportAs
					,Flags
					,FullName
					,IsBillable
					,Name1
					,Name2
					,Name3
					,Name4
					,Name5
					,Notes
					,Stage
					,Status
					,UTCDateAdded
					,UTCDateModified
					)
					SELECT DISTINCT
						(
						  SELECT
							MAX(cc.RecordId) + 1
						  FROM
							tcp_EmployeeWork.CostCode AS cc
						)
					   ,NULL
					   ,NULL
					   ,NULL
					   ,1
					   ,NULL
					   ,NULL
					   ,LEFT(COALESCE(REPLACE(pva.name , '''' , '''''') , 'T.J. Snow') , 60)
					   ,0
					   ,pvpol.item_id
					   ,0
					   ,CASE WHEN pvim.item_id LIKE 'RP0%' THEN 'REPAIRS'
							 WHEN pvim.item_id LIKE 'NM0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'NMW0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'UM0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'UMW0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'TJM%' THEN 'TJM'
							 WHEN pvim.item_id LIKE 'INSTALLATION - SERVICE%' THEN 'SERVICE'
							 WHEN pvim.item_id LIKE 'SEMINAR%' THEN 'SERVICE'
							 ELSE 'OTHER'
						END + '\' + ( pvim.item_id + ' | ' + CAST(pvpol.prod_order_number AS VARCHAR(20)) )
					   ,0
					   ,CASE WHEN pvim.item_id LIKE 'RP0%' THEN 'REPAIRS'
							 WHEN pvim.item_id LIKE 'NM0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'NMW0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'UM0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'UMW0%' THEN 'MACHINES'
							 WHEN pvim.item_id LIKE 'TJM%' THEN 'TJM'
							 WHEN pvim.item_id LIKE 'INSTALLATION - SERVICE%' THEN 'SERVICE'
							 WHEN pvim.item_id LIKE 'SEMINAR%' THEN 'SERVICE'
							 ELSE 'OTHER'
						END
					   ,pvpol.item_id + ' | ' + CAST(pvpol.prod_order_number AS VARCHAR(20))
					   ,NULL
					   ,NULL
					   ,NULL
					   ,pvpol.prod_order_number
					   ,0
					   ,0
					   ,GETUTCDATE()
					   ,NULL
					FROM
						P21..p21_view_prod_order_line AS pvpol
						LEFT JOIN P21..p21_view_prod_order_line_link AS pvpoll ON pvpoll.prod_order_number = pvpol.prod_order_number
																					  AND pvpoll.prod_order_line_number = pvpol.line_number
						LEFT JOIN P21..p21_view_oe_line AS pvol ON pvol.oe_line_uid = pvpoll.transaction_uid
						INNER JOIN P21..p21_view_inv_mast AS pvim ON pvim.inv_mast_uid = pvpol.inv_mast_uid
						LEFT JOIN P21..p21_view_oe_hdr AS pvoh ON pvol.order_no = pvoh.order_no
						LEFT JOIN P21..p21_view_address AS pva ON pvoh.customer_id = pva.id
					WHERE
						pvpol.prod_order_number = @prod
						AND pvpol.item_id = @item
					ORDER BY
						pvpol.item_id;

			UPDATE
				P21..prod_ord_tickler_tjs
			SET	
				P21..prod_ord_tickler_tjs.processed = 'Y'
			WHERE
				P21..prod_ord_tickler_tjs.uid = @id;
		END;


DECLARE	@temp TABLE
	(
	 uid INT IDENTITY
	,ccri INT
	,processed VARCHAR(1) DEFAULT 'N'
	);

INSERT	INTO @temp
		(
		 ccri
		,processed
			
		)
		SELECT
			cc.RecordId
		   ,'N'
		FROM
			tcp_EmployeeWork.CostCode AS cc
		WHERE
			cc.ExportAs IS NOT NULL
			AND cc.RecordId NOT IN ( SELECT
										COALESCE(ccn.CostCodeRecordId , '')
									 FROM
										tcp_EmployeeWork.CostCodeNode AS ccn );

WHILE EXISTS ( SELECT
				*
			   FROM
				@temp AS t
			   WHERE
				t.processed = 'N' )
	BEGIN
		DECLARE	@uid INT;
		DECLARE	@ccri INT;
		SET @uid = (
					 SELECT TOP 1
						t.uid
					 FROM
						@temp AS t
					 WHERE
						t.processed = 'N'
					 ORDER BY
						t.uid
				   );
		SET @ccri = (
					  SELECT
						t.ccri
					  FROM
						@temp AS t
					  WHERE
						t.uid = @uid
					);

		INSERT	INTO tcp_EmployeeWork.CostCodeNode
				(
				 RecordId
				,CompanyRecordId
				,CostCodeRecordId
				,Level
				,Name
				,ParentRecordId
				
					
				)
				SELECT DISTINCT
					(
					  SELECT
						MAX(ccn.RecordId) + 1
					  FROM
						tcp_EmployeeWork.CostCodeNode AS ccn
					)
				   ,1
				   ,cc.RecordId
				   ,2
				   ,cc.Name2
				   ,ccn2.RecordId
				FROM
					tcp_EmployeeWork.CostCode AS cc
					LEFT JOIN tcp_EmployeeWork.CostCodeNode AS ccn2 ON ccn2.CompanyRecordId = cc.CompanyRecordId
																	   AND cc.Name1 = ccn2.Name
				WHERE
					cc.RecordId = @ccri;
					
		UPDATE
			@temp
		SET	
			[@temp].processed = 'Y'
		WHERE
			[@temp].uid = @uid;
	END;
	
-- $History:$
--
--
GO